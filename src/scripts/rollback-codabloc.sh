#!/bin/sh

set -e

# console/notebook from $0
target=$(echo $0 | cut -f2 -d'-' | cut -f1 -d'.')
# current revision
current=$(readlink $target | sed 's:/*$::')
bug_tag="buggy-"

# get old revision
old=$(find ${target}_revisions/ -maxdepth 1 -mindepth 1 -type d -not -name "${bug_tag}*" | sort | grep -B1 $current | head -n1)

# replace symlink
ln -snf "$old" $target

# move buggy revision
mv $current $(dirname $current)/$bug_tag$(basename $current)
