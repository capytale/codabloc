import { openModal, closeModal } from "../../reducers/modals";

const ENABLE_PROJECT_SAVING = "scratch-gui/capytale/ENABLE_PROJECT_SAVING";
const DISABLE_PROJECT_SAVING = "scratch-gui/capytale/DISABLE_PROJECT_SAVING";

const OPEN_CAPYTALE_MODAL = "scratch-gui/capytale/OPEN_CAPYTALE_MODAL";
const CLOSE_CAPYTALE_MODAL = "scratch-gui/capytale/CLOSE_CAPYTALE_MODAL";

const SET_MODE = "scratch-gui/capytale/SET_MODE";
const SET_CREATE_MODE = "scratch-gui/capytale/SET_CREATE_MODE";
const SET_WORKFLOW = "scratch-gui/capytale/SET_WORKFLOW";
const SET_TITLE = "scratch-gui/capytale/SET_TITLE";
const SET_DESCRIPTION_CONTENT = "scratch-gui/capytale/SET_DESCRIPTION_CONTENT";
const SET_APPRECIATION_CONTENT =
    "scratch-gui/capytale/SET_APPRECIATION_CONTENT";
const SET_EVALUATION_CONTENT = "scratch-gui/capytale/SET_EVALUATION_CONTENT";
const SET_BACK_LINK = "scratch-gui/capytale/SET_BACK_LINK";
const SET_STUDENT_NAME = "scratch-gui/capytale/SET_STUDENT_NAME";
const CAN_SAVE = "canSave";
const MODE = "mode";
const CREATE_MODE = "createMode";
const WORKFLOW = "workflow";
const TITLE = "title";
const MODAL_DESCRIPTION = "descriptionModal";
const MODAL_DESCRIPTION_CONTENT = "descriptionContent";
const MODAL_EVALUATION_APPRECIATION_CONTENT = "appreciationContent";
const MODAL_EVALUATION_EVALUATION_CONTENT = "evaluationContent";
const MODAL_EVALUATION = "evaluationModal";
const MODAL_WORKFLOW = "workflowModal";
const MODAL_ABOUT = "aboutModal";
const BACK_LINK = "backLink";
const STUDENT_NAME = "studentName";

const initialState = {
    [CAN_SAVE]: true,
    [MODE]: undefined,
    [CREATE_MODE]: undefined,
    [WORKFLOW]: undefined,
    [TITLE]: "Consigne",
    [MODAL_DESCRIPTION]: false,
    [MODAL_DESCRIPTION_CONTENT]: "Oups, la consigne n'a pas été chargée...",
    [MODAL_EVALUATION_APPRECIATION_CONTENT]:
        "Oups, l'appréciation n'a pas été chargée...",
    [MODAL_EVALUATION_EVALUATION_CONTENT]:
        "Oups, l'évaluation n'a pas été chargée...",
    [MODAL_EVALUATION]: false,
    [MODAL_WORKFLOW]: false,
    [MODAL_ABOUT]: false,
    [BACK_LINK]: "",
    [STUDENT_NAME]: "",
};

/**
 * Capytale reducer (see reducers/gui.js).
 */
const reducer = function (state, action) {
    if (typeof state === "undefined") state = initialState;
    switch (action.type) {
        case ENABLE_PROJECT_SAVING:
            return Object.assign({}, state, {
                [CAN_SAVE]: true,
            });
        case DISABLE_PROJECT_SAVING:
            return Object.assign({}, state, {
                [CAN_SAVE]: false,
            });
        case OPEN_CAPYTALE_MODAL:
            return Object.assign({}, state, {
                [action.modal]: true,
            });
        case CLOSE_CAPYTALE_MODAL:
            return Object.assign({}, state, {
                [action.modal]: false,
            });
        case SET_MODE:
            return Object.assign({}, state, {
                [MODE]: action.mode,
            });
        case SET_CREATE_MODE:
            return Object.assign({}, state, {
                [CREATE_MODE]: action.createMode,
            });
        case SET_WORKFLOW:
            return Object.assign({}, state, {
                [WORKFLOW]: action.workflow,
            });
        case SET_TITLE:
            return Object.assign({}, state, {
                [TITLE]: action.title,
            });
        case SET_DESCRIPTION_CONTENT:
            return Object.assign({}, state, {
                [MODAL_DESCRIPTION_CONTENT]: action.content,
            });
        case SET_APPRECIATION_CONTENT:
            return Object.assign({}, state, {
                [MODAL_EVALUATION_APPRECIATION_CONTENT]: action.content,
            });
        case SET_EVALUATION_CONTENT:
            return Object.assign({}, state, {
                [MODAL_EVALUATION_EVALUATION_CONTENT]: action.content,
            });
        case SET_BACK_LINK:
            return Object.assign({}, state, {
                [BACK_LINK]: action.link,
            });
        case SET_STUDENT_NAME:
            return Object.assign({}, state, {
                [STUDENT_NAME]: action.name,
            });
        default:
            return state;
    }
};

const openCapytaleModal = function (modal) {
    return {
        type: OPEN_CAPYTALE_MODAL,
        modal,
    };
};

const closeCapytaleModal = function (modal) {
    return {
        type: CLOSE_CAPYTALE_MODAL,
        modal,
    };
};

/**
 * Enable project saving (to dispatch).
 */
const enableProjectSaving = function () {
    return { type: ENABLE_PROJECT_SAVING };
};

/**
 * Disable project saving (to dispatch).
 */
const disableProjectSaving = function () {
    return { type: DISABLE_PROJECT_SAVING };
};

/**
 * Set the mode (to dispatch).
 */
const setMode = function (mode) {
    return {
        type: SET_MODE,
        mode,
    };
};

/**
 * Set the create mode (to dispatch).
 */
const setCreateMode = function (createMode) {
    return {
        type: SET_CREATE_MODE,
        createMode,
    };
};

/**
 * Set the workflow (to dispatch).
 */
const setWorkflow = function (workflow) {
    return {
        type: SET_WORKFLOW,
        workflow,
    };
};

/**
 * Set the title (to dispatch).
 */
const setTitle = function (title) {
    return {
        type: SET_TITLE,
        title,
    };
};

/**
 * Open the description modal (to dispatch).
 */
const openDescriptionModal = function () {
    return openCapytaleModal(MODAL_DESCRIPTION);
};

/**
 * Set the description content (to dispatch).
 */
const setDescriptionContent = function (content) {
    return {
        type: SET_DESCRIPTION_CONTENT,
        content,
    };
};

/**
 * Set the appreciation content (to dispatch).
 */
const setAppreciationContent = function (content) {
    return {
        type: SET_APPRECIATION_CONTENT,
        content,
    };
};

/**
 * Set the evaluation content (to dispatch).
 */
const setEvaluationContent = function (content) {
    return {
        type: SET_EVALUATION_CONTENT,
        content,
    };
};

/**
 * Open the evaluation modal (to dispatch).
 */
const openEvaluationModal = function () {
    return openCapytaleModal(MODAL_EVALUATION);
};

/**
 * Open the workflow modal (to dispatch).
 */
const openWorkflowModal = function () {
    return openCapytaleModal(MODAL_WORKFLOW);
};

/**
 * Open the about modal (to dispatch).
 */
const openAboutModal = function () {
    return openCapytaleModal(MODAL_ABOUT);
};

/**
 * Set the link of the back button (to dispatch).
 */
const setBackLink = function (link) {
    return {
        type: SET_BACK_LINK,
        link,
    };
};

/**
 * Set the student's name (to dispatch).
 */
const setStudentName = function (name) {
    return {
        type: SET_STUDENT_NAME,
        name,
    };
};

/**
 * Close the description modal (to dispatch).
 */
const closeDescriptionModal = function () {
    return closeCapytaleModal(MODAL_DESCRIPTION);
};

/**
 * Close the evaluation modal (to dispatch).
 */
const closeEvaluationModal = function () {
    return closeCapytaleModal(MODAL_EVALUATION);
};

/**
 * Close the workflow modal (to dispatch).
 */
const closeWorkflowModal = function () {
    return closeCapytaleModal(MODAL_WORKFLOW);
};

/**
 * Close the about modal (to dispatch).
 */
const closeAboutModal = function () {
    return closeCapytaleModal(MODAL_ABOUT);
};

export {
    reducer as capytaleReducer,
    initialState as capytaleInitialState,
    enableProjectSaving,
    disableProjectSaving,
    openDescriptionModal,
    openEvaluationModal,
    openWorkflowModal,
    openAboutModal,
    setMode,
    setCreateMode,
    setWorkflow,
    setTitle,
    setDescriptionContent,
    setAppreciationContent,
    setEvaluationContent,
    setBackLink,
    setStudentName,
    closeDescriptionModal,
    closeEvaluationModal,
    closeWorkflowModal,
    closeAboutModal,
};
