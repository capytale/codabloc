"use strict";
var xhr;
(function (xhr) {
    class network_error extends Error {
        constructor(msg) {
            super(msg);
            this.name = 'network_error';
        }
    }
    xhr.network_error = network_error;
    class status_error extends Error {
        constructor(msg, status) {
            super(msg);
            this.name = 'status_error';
            this.status = status;
        }
    }
    xhr.status_error = status_error;
    function rawRequestAsync(method, url, data, hdr, expectedStatus = [200]) {
        if (method !== "PUT" && data && typeof data !== 'string') {
            data = JSON.stringify(data);
        }
        let request = new Request(url, {
            method: method,
            headers: hdr,
            body: data,
        });
        return fetch(request).then((response) => {
            if (expectedStatus.includes(response.status)) {
                return response;
            }
            else {
                let msg = method + ' ' + url + ' : echec avec le statut ' + response.status + ' (' + response.statusText + ')';
                console.log(msg);
                return response.json().then(dt => {
                    if (dt.message !== undefined) {
                        let drupal_msg = 'Message drupal : ' + dt.message;
                        console.log(drupal_msg);
                        throw new status_error(msg + '\r' + drupal_msg, response.status);
                    }
                    else {
                        throw new status_error(msg, response.status);
                    }
                }, e => {
                    throw new status_error(msg, response.status);
                });
            }
        }, e => {
            console.log(e);
            throw new network_error(e.message);
        });
    }
    xhr.rawRequestAsync = rawRequestAsync;
    function requestAsync(method, url, data, hdr, repType, expectedStatus = [200]) {
        return rawRequestAsync(method, url, data, hdr, expectedStatus).then((response) => {
            let respPromise;
            if (repType === 'json')
                respPromise = response.json();
            else
                respPromise = response.text();
            return respPromise.catch((e) => {
                console.log(e);
                throw e;
            });
        });
    }
    function getJsonAsync(url, hdr) {
        return requestAsync("GET", url, null, hdr, "json");
    }
    xhr.getJsonAsync = getJsonAsync;
    function getStringAsync(url, hdr) {
        return requestAsync("GET", url, null, hdr, "text");
    }
    xhr.getStringAsync = getStringAsync;
    function getStringOrNullAsync(url, hdr) {
        return rawRequestAsync("GET", url, null, hdr, [200, 204]).then((response) => {
            if (response.status === 204)
                return null;
            return response.text().catch((e) => {
                console.log(e);
                throw e;
            });
        });
    }
    xhr.getStringOrNullAsync = getStringOrNullAsync;
    function deleteAsync(url, hdr) {
        return rawRequestAsync("DELETE", url, null, hdr, [204]).then(() => { });
    }
    xhr.deleteAsync = deleteAsync;
    function putAsync(url, data, hdr) {
        return rawRequestAsync("PUT", url, data, hdr, [204]).then(() => { });
    }
    xhr.putAsync = putAsync;
    function patchGetJsonAsync(url, data, hdr) {
        return requestAsync("PATCH", url, data, hdr, "json");
    }
    xhr.patchGetJsonAsync = patchGetJsonAsync;
    function patchGetStringAsync(url, data, hdr) {
        return requestAsync("PATCH", url, data, hdr, "text");
    }
    xhr.patchGetStringAsync = patchGetStringAsync;
    function postGetJsonAsync(url, data, hdr) {
        return requestAsync("POST", url, data, hdr, "json");
    }
    xhr.postGetJsonAsync = postGetJsonAsync;
    function postGetStringAsync(url, data, hdr) {
        return requestAsync("POST", url, data, hdr, "text");
    }
    xhr.postGetStringAsync = postGetStringAsync;
})(xhr || (xhr = {}));
var api;
(function (api) {
    api.API_ACCESS_HEADER = { 'X-API-ACCESS': 'capytale' };
    let csrf_tk_promise;
    function get_csrf_tk_async() {
        if (undefined !== csrf_tk_promise)
            return csrf_tk_promise;
        return csrf_tk_promise = xhr.getStringAsync('/web/session/token')
            .catch(reason => {
            csrf_tk_promise = undefined;
            return Promise.reject(reason);
        });
    }
    api.get_csrf_tk_async = get_csrf_tk_async;
    function add_csrf_headers_async(hdr) {
        return get_csrf_tk_async().then(csrf_tk => {
            hdr['X-CSRF-Token'] = csrf_tk;
            return hdr;
        });
    }
    api.add_csrf_headers_async = add_csrf_headers_async;
    function clear_csrf_tk() {
        csrf_tk_promise = undefined;
    }
    api.clear_csrf_tk = clear_csrf_tk;
})(api || (api = {}));
var api;
(function (api) {
    var fields;
    (function (fields) {
        const APIPATH = "/web/c-act/api/";
        function fields_url(id) {
            return APIPATH + 'n/' + id + '/fields';
        }
        fields.fields_url = fields_url;
        function field_url(id, field) {
            return fields_url(id) + '/' + field;
        }
        fields.field_url = field_url;
        function get_fields_async(id) {
            return xhr.getJsonAsync(fields_url(id));
        }
        fields.get_fields_async = get_fields_async;
        function get_field_async(id, field) {
            return xhr.getStringOrNullAsync(field_url(id, field), api.API_ACCESS_HEADER);
        }
        fields.get_field_async = get_field_async;
        function put_field_async(id, field, data) {
            if (null === data) {
                return api.add_csrf_headers_async({}).then(hdr => xhr.deleteAsync(field_url(id, field), hdr));
            }
            else {
                return api.add_csrf_headers_async({}).then(hdr => xhr.putAsync(field_url(id, field), data, hdr));
            }
        }
        fields.put_field_async = put_field_async;
    })(fields = api.fields || (api.fields = {}));
})(api || (api = {}));
var api;
(function (api) {
    var rest;
    (function (rest) {
        class rest_error extends Error {
            constructor(msg) {
                super(msg);
                this.name = 'rest_error';
            }
        }
        rest.rest_error = rest_error;
        function value_extractor(o) {
            if (undefined === o)
                return undefined;
            if (o.value)
                return o.value;
            return null;
        }
        rest.value_extractor = value_extractor;
        function value_encapsulator(v) {
            return { 'value': v };
        }
        rest.value_encapsulator = value_encapsulator;
        function target_id_extractor(o) {
            if (o.target_id)
                return o.target_id;
            return null;
        }
        rest.target_id_extractor = target_id_extractor;
        function target_id_encapsulator(v) {
            return { 'target_id': v };
        }
        rest.target_id_encapsulator = target_id_encapsulator;
        function url_extractor(o) {
            if (o.url)
                return o.url;
            return null;
        }
        rest.url_extractor = url_extractor;
        function url_encapsulator(v) {
            return { 'url': v };
        }
        rest.url_encapsulator = url_encapsulator;
        function extract_single(dt, extractor) {
            let l = dt.length;
            if (l === 0) {
                return null;
            }
            if (l === 1) {
                return extractor(dt[0]);
            }
            throw new Error("Single value expected but array received.");
        }
        rest.extract_single = extract_single;
        function encapsulate_single(v, encapsulator) {
            if ((null === v) || (undefined === v))
                return [];
            return [encapsulator(v)];
        }
        rest.encapsulate_single = encapsulate_single;
        function extract_single_value(dt) {
            return extract_single(dt, value_extractor);
        }
        rest.extract_single_value = extract_single_value;
        function encapsulate_single_value(v) {
            return encapsulate_single(v, value_encapsulator);
        }
        rest.encapsulate_single_value = encapsulate_single_value;
        function extract_single_target_id(dt) {
            return extract_single(dt, target_id_extractor);
        }
        rest.extract_single_target_id = extract_single_target_id;
        function encapsulate_single_target_id(v) {
            return encapsulate_single(v, target_id_encapsulator);
        }
        rest.encapsulate_single_target_id = encapsulate_single_target_id;
        function extract_array(dt, extractor) {
            let vals = [];
            for (var item of dt) {
                vals.push(extractor(item));
            }
            return vals;
        }
        rest.extract_array = extract_array;
        function encapsulate_array(v, encapsulator) {
            let vals = [];
            for (var item of v) {
                vals.push(encapsulator(item));
            }
            return vals;
        }
        rest.encapsulate_array = encapsulate_array;
        function getAsync(url) {
            return xhr.getJsonAsync(url);
        }
        rest.getAsync = getAsync;
        function getOneAsync(url) {
            return xhr.getJsonAsync(url)
                .then(o => {
                if (!Array.isArray(o))
                    throw new rest_error('Drupal n\'a pas renvoyé un tableau.');
                if (o.length === 0)
                    return false;
                return o[0];
            });
        }
        rest.getOneAsync = getOneAsync;
        function saveAsync(url, o) {
            return api.add_csrf_headers_async({ 'Content-Type': 'application/json' })
                .then(hdr => {
                return xhr.patchGetJsonAsync(url, o, hdr);
            });
        }
        rest.saveAsync = saveAsync;
    })(rest = api.rest || (api.rest = {}));
})(api || (api = {}));
var user;
(function (user_1) {
    const meUrl = '/web/c-auth/api/me?_format=json';
    function userinfos_url(uid) {
        return '/web/userinfos?uid_raw=' + uid + '&_format=json';
    }
    class not_connected_error extends Error {
        constructor(msg) {
            super(msg);
            this.name = 'not_connected_error';
        }
    }
    user_1.not_connected_error = not_connected_error;
    class user {
        constructor(o) {
            try {
                this.uid = api.rest.extract_single(o['uid'], api.rest.value_extractor);
            } catch (e) {
                this.uid = o['uid'];
            }
            try {
                this.firstname = api.rest.extract_single(o['field_prenom'], api.rest.value_extractor);
                this.lastname = api.rest.extract_single(o['field_nom'], api.rest.value_extractor);
                this.classe = api.rest.extract_single(o['field_classe'], api.rest.value_extractor);
            } catch (e) {}
            try {
                this.muuid = o['muuid'];
            } catch (e) {}
        }
    }
    user_1.user = user;
    function get_me() {
        return api.rest.getAsync(meUrl)
            .then(o => {
            if (false === o)
                throw new not_connected_error('Il n\'y a pas de session Capytale active.');
            return new user(o);
        });
    }
    user_1.get_me = get_me;
    function get_infos(uid) {
        return api.rest.getOneAsync(userinfos_url(uid))
            .then(o => {
            if (false === o)
                return false;
            return new user(o);
        });
    }
    user_1.get_infos = get_infos;
})(user || (user = {}));
var clip_board;
(function (clip_board) {
    function createNode(text) {
        const node = document.createElement('pre');
        node.style.width = '1px';
        node.style.height = '1px';
        node.style.position = 'fixed';
        node.style.top = '5px';
        node.textContent = text;
        return node;
    }
    function copyNode(node) {
        if ('clipboard' in navigator) {
            return navigator.clipboard.writeText(node.textContent);
        }
        const selection = getSelection();
        if (selection == null) {
            return Promise.reject(new Error());
        }
        selection.removeAllRanges();
        const range = document.createRange();
        range.selectNodeContents(node);
        selection.addRange(range);
        document.execCommand('copy');
        selection.removeAllRanges();
        return Promise.resolve();
    }
    function copyText(text) {
        if ('clipboard' in navigator) {
            return navigator.clipboard.writeText(text);
        }
        const body = document.body;
        if (!body) {
            return Promise.reject(new Error());
        }
        const node = createNode(text);
        body.appendChild(node);
        copyNode(node);
        body.removeChild(node);
        return Promise.resolve();
    }
    clip_board.copyText = copyText;
    function wire_copy_btn(btn, text) {
        btn.addEventListener('click', () => {
            clip_board.copyText(text).then(() => {
                btn.classList.add('copy-check');
                setTimeout(() => {
                    btn.classList.remove('copy-check');
                }, 500);
            });
        });
    }
    clip_board.wire_copy_btn = wire_copy_btn;
    function create_copy_btn(text) {
        const btn = document.createElement('button');
        btn.classList.add('copy-btn');
        let i = document.createElement('i');
        i.classList.add('copy-copy', 'far', 'fa-copy');
        btn.appendChild(i);
        i = document.createElement('i');
        i.classList.add('copy-check', 'fas', 'fa-check');
        btn.appendChild(i);
        wire_copy_btn(btn, text);
        return btn;
    }
    clip_board.create_copy_btn = create_copy_btn;
})(clip_board || (clip_board = {}));
var node;
(function (node) {
    class node_error extends Error {
        constructor(msg) {
            super(msg);
            this.name = 'node_error';
        }
    }
    node.node_error = node_error;
})(node || (node = {}));
var node;
(function (node) {
    function node_url(id = null) {
        if (null == id) {
            return '/web/node?_format=json';
        }
        else {
            return '/web/node/' + id + '?_format=json';
        }
    }
    node.node_url = node_url;
    function is_ro_type(t) {
        return (t === "ro") || (t === "target") || (t === "roid") || (t === "file");
    }
    function drupal_field_name(pn) { return "field_" + pn; }
    node.drupal_field_name = drupal_field_name;
    function nes(s) { return (s === "") ? "\n" : s; }
    function nesa(as) {
        let res = [];
        for (let s of as)
            res.push(nes(s));
        return res;
    }
    function default_value(def) {
        let val = def.default;
        if (def.is_array) {
            if (undefined === val)
                return [];
            else
                return val;
        }
        return val;
    }
    function get_extractor(def) {
        switch (def.type) {
            case 'target':
            case 'roid':
                return api.rest.target_id_extractor;
            case 'file':
                return api.rest.url_extractor;
            default:
                return api.rest.value_extractor;
        }
    }
    function get_encapsulator(def) {
        if ((def.type === "target") || (def.type === "roid") || (def.type === "file"))
            throw new node.node_error("Set target or file not yet supported.");
        else
            return api.rest.value_encapsulator;
    }
    function extract_data(dt, def) {
        if (def.is_array) {
            return api.rest.extract_array(dt, get_extractor(def));
        }
        else {
            return api.rest.extract_single(dt, get_extractor(def));
        }
    }
    function encapsulate_data(dt, def) {
        if (def.is_array) {
            return api.rest.encapsulate_array(dt, get_encapsulator(def));
        }
        else {
            return api.rest.encapsulate_single(dt, get_encapsulator(def));
        }
    }
    class node_object {
        constructor(o) {
            this.__dirty_change_handler = () => { };
            if (null != o) {
                this.__nid = api.rest.extract_single_value(o.nid);
                this.load_fields(o);
            }
            this.__dirty_v = false;
            this.__modif_values = {};
            this.__saving_values = {};
            this.__modif_title = undefined;
            this.__saving_title = undefined;
            this.__saving = false;
        }
        set __dirty(v) {
            if (this.__dirty_v !== v) {
                this.__dirty_v = v;
                this.__dirty_change_handler(v);
            }
        }
        set on_dirty_change(h) { this.__dirty_change_handler = h; }
        get is_dirty() { return this.__dirty_v || this.__saving; }
        get has_dirty_targets() {
            if (!this.__loaded_targets)
                return false;
            for (let pn of this.__targets_pn) {
                let def = this.__get_field_def(pn);
                let v = this.__loaded_targets[pn];
                if (def.is_array) {
                    for (let t of v) {
                        if (t.is_dirty)
                            return true;
                        if (t.has_dirty_targets)
                            return true;
                    }
                }
                else {
                    if (v.is_dirty)
                        return true;
                    if (v.has_dirty_targets)
                        return true;
                }
            }
            return false;
        }
        __get_field_def(pn) {
            let def = this.__def.fields[pn];
            if (!def)
                throw new Error("Unknown field " + pn + ".");
            return def;
        }
        __get(pn) {
            let def = this.__get_field_def(pn);
            if (def.type === "target")
                throw new node.node_error("target vs field " + pn);
            let val = this.__modif_values[pn];
            if (undefined !== val)
                return val;
            val = this.__saving_values[pn];
            if (undefined !== val)
                return val;
            val = this.__loaded_values[pn];
            if (null !== val)
                return val;
            return default_value(def);
        }
        __get_raw(pn) {
            let def = this.__get_field_def(pn);
            if (def.type === "target")
                throw new node.node_error("target vs field " + pn);
            let val = this.__modif_values[pn];
            if (undefined !== val)
                return val;
            val = this.__saving_values[pn];
            if (undefined !== val)
                return val;
            return this.__loaded_values[pn];
        }
        __get_target(pn) {
            if (!this.__loaded_targets)
                throw new node.node_error("Targets not loaded.");
            let def = this.__get_field_def(pn);
            if (def.type !== "target")
                throw new node.node_error("target vs field " + pn);
            let val = this.__loaded_targets[pn];
            if (undefined !== val)
                return val;
            return default_value(def);
        }
        __set(pn, v) {
            let def = this.__get_field_def(pn);
            if (is_ro_type(def.type))
                throw new node.node_error("Field " + pn + "is read only.");
            if (def.is_array) {
                if (!Array.isArray(v))
                    throw new node.node_error("Array value expected for field " + pn + ".");
                if (def.type === "wnes")
                    v = nesa(v);
            }
            else {
                if (def.type === "wnes")
                    v = nes(v);
            }
            this.__dirty = true;
            this.__modif_values[pn] = v;
        }
        __set_target(pn, v) {
            throw new node.node_error("Set target not yet implemented.");
        }
        get type() { return this.__def.type; }
        get uid() { return this.__uid; }
        get nid() { return this.__nid; }
        get title() { return this.__modif_title || this.__saving_title || this.__title; }
        set title(v) {
            if (this.__def.ro_title)
                throw new node.node_error("Title is read only.");
            this.__dirty = true;
            this.__modif_title = v;
        }
        load_fields(o) {
            this.__uid = api.rest.extract_single_target_id(o.uid);
            this.__title = api.rest.extract_single_value(o.title);
            let lv = this.__loaded_values = {};
            for (var pn in this.__def.fields) {
                this.load_field(o, pn, lv);
            }
            this.cancel_modifs();
        }
        load_field(o, pn, lv) {
            let def = this.__def.fields[pn];
            let dpn = drupal_field_name(pn);
            let dt = o[dpn];
            if (!dt)
                throw new node.node_error("Field " + pn + " was not received.");
            lv[pn] = extract_data(dt, def);
        }
        cancel_modifs() {
            if (this.__saving)
                throw new node.node_error('Impossible to cancel while saving operation is in progress.');
            this.__dirty = false;
            this.__modif_values = {};
            this.__modif_title = undefined;
        }
        async load_targets(recurse = false) {
            let lt = this.__loaded_targets = {};
            if (!this.has_targets)
                return;
            for (var pn of this.__targets_pn) {
                await this.load_target(pn, lt, recurse);
            }
        }
        load_target(pn, lt, recurse = false) {
            let def = this.__def.fields[pn];
            let target_id = this.__loaded_values[pn];
            if (def.is_array) {
                return node.load_array(target_id, recurse, recurse)
                    .then(na => { lt[pn] = na; });
            }
            else {
                return node.load(target_id, recurse, recurse)
                    .then(n => { lt[pn] = n; });
            }
        }
        build_modif_o() {
            let m = {
                nid: api.rest.encapsulate_single_value(this.nid),
                type: api.rest.encapsulate_single_target_id(this.type),
            };
            if (this.__saving_title)
                m.title = api.rest.encapsulate_single_value(this.__saving_title);
            let mv = this.__saving_values;
            for (var pn in mv) {
                let def = this.__get_field_def(pn);
                let dpn = drupal_field_name(pn);
                m[dpn] = encapsulate_data(mv[pn], def);
            }
            return m;
        }
        async save(fields = "all") {
            if (!this.__dirty_v)
                return;
            if (this.__saving)
                throw new node.node_error('Save operation already in progress.');
            let partial = false;
            if ("all" === fields) {
                this.__saving_values = this.__modif_values;
                this.__modif_values = {};
                this.__saving_title = this.__modif_title;
                this.__modif_title = undefined;
                this.__dirty = false;
            }
            else {
                let len = fields.length;
                if (0 === len)
                    return;
                for (let i = 0; i < len; i++) {
                    let pn = fields[i];
                    if ("title" === pn) {
                        if (this.__def.ro_title)
                            throw new node.node_error("Title is read only.");
                    }
                    else {
                        let def = this.__get_field_def(pn);
                        if (is_ro_type(def.type))
                            throw new node.node_error("Field " + pn + "is read only.");
                    }
                }
                let has_to_save = false;
                this.__saving_values = {};
                for (let i = 0; i < len; i++) {
                    let pn = fields[i];
                    if ("title" === pn) {
                        this.__saving_title = this.__modif_title;
                        this.__modif_title = undefined;
                        has_to_save = true;
                    }
                    else {
                        let def = this.__get_field_def(pn);
                        if (typeof this.__modif_values[pn] !== 'undefined') {
                            this.__saving_values[pn] = this.__modif_values[pn];
                            has_to_save = true;
                        }
                        delete (this.__modif_values[pn]);
                    }
                }
                if (!has_to_save)
                    return;
                if (typeof this.__modif_title === 'undefined') {
                    for (let p in this.__modif_values) {
                        partial = true;
                        break;
                    }
                }
                else {
                    partial = true;
                }
                if (!partial) {
                    this.__dirty = false;
                }
            }
            this.__saving = true;
            try {
                let o = await api.rest.saveAsync(node_url(this.nid), this.build_modif_o());
                this.__saving = false;
                this.load_fields(o);
                this.__saving_values = {};
                this.__saving_title = undefined;
            }
            catch (_a) {
                this.__saving = false;
                if ((typeof this.__modif_title === 'undefined') && (typeof this.__saving_title !== 'undefined')) {
                    this.__modif_title = this.__saving_title;
                }
                this.__saving_title = undefined;
                let mv = this.__saving_values;
                for (var pn in mv) {
                    if ((typeof this.__modif_values[pn] === 'undefined') && (typeof this.__saving_values[pn] !== 'undefined')) {
                        this.__modif_values[pn] = this.__saving_values[pn];
                    }
                }
                this.__saving_values = {};
                this.__dirty = true;
                throw new node.node_error('Save operation failed.');
            }
        }
    }
    node.node_object = node_object;
})(node || (node = {}));
var node;
(function (node) {
    let classes_map = {};
    function create_prop_map(cl) {
        let node_fields = cl.def.fields;
        let map = {
            '__def': { value: cl.def, writable: false },
        };
        let has_targets = false;
        let targets = [];
        for (let pn in node_fields) {
            let def = node_fields[pn];
            def.is_array = !!def.is_array;
            if (def.type === "target") {
                targets.push(pn);
                has_targets = true;
                map[pn] = {
                    get: function () { return this.__get_target(pn); },
                    set: function (v) { this.__set_target(pn, v); },
                };
            }
            else {
                map[pn] = {
                    get: function () { return this.__get(pn); },
                    set: function (v) { this.__set(pn, v); },
                };
            }
        }
        map['__targets_pn'] = { value: targets, writable: false };
        map['has_targets'] = { value: has_targets, writable: false };
        return map;
    }
    function declare_node(cl) {
        classes_map[cl.def.type] = cl;
        if (cl.def.ro_title === undefined)
            cl.def.ro_title = true;
        Object.defineProperties(cl.prototype, create_prop_map(cl));
    }
    node.declare_node = declare_node;
    async function load(id, load_targets = false, recurse = false) {
        let o = await api.rest.getAsync(node.node_url(id));
        let t = api.rest.extract_single_target_id(o.type);
        let cl = classes_map[t];
        if (!cl)
            throw new node.node_error("Unknown node type " + t + ".");
        let n = new cl(o);
        if (load_targets) {
            await n.load_targets(recurse);
        }
        return n;
    }
    node.load = load;
    async function load_array(ids, load_targets = false, recurse = false) {
        let res = [];
        for (let id of ids) {
            res.push(await load(id, load_targets, recurse));
        }
        return res;
    }
    node.load_array = load_array;
})(node || (node = {}));
var activity;
(function (activity) {
    class activity_error extends Error {
        constructor(msg) {
            super(msg);
            this.name = 'activity_error';
        }
    }
    activity.activity_error = activity_error;
    class user_changed_error extends Error {
        constructor(msg) {
            super(msg);
            this.name = 'user_changed_error';
        }
    }
    activity.user_changed_error = user_changed_error;
})(activity || (activity = {}));
var activity;
(function (activity) {
    class node_root extends node.node_object {
    }
    activity.node_root = node_root;
})(activity || (activity = {}));
var activity;
(function (activity) {
    class node_activity extends activity.node_root {
        is_owner(uid) {
            return (uid === this.uid);
        }
        is_associate(uid) {
            return this.associates.includes(uid);
        }
    }
    node_activity.def = {
        fields: {
            'config': { type: "w", default: "{}" },
            'description': { type: "w", default: "" },
            'status_clonable': { type: "w", default: false },
            'status_shared': { type: "w", default: false },
            'code': { type: "ro", default: "??" },
            'associates': { type: "roid", default: [], is_array: true },
            'attached_files': { type: "file", default: [], is_array: true },
        },
        ro_title: false,
    };
    activity.node_activity = node_activity;
    class generic_activity extends node_activity {
    }
    generic_activity.def = {
        type: "activity",
        fields: Object.assign(Object.assign({}, activity.node_activity.def.fields), { 'activity_type': { type: "ro" } }),
        ro_title: activity.node_activity.def.ro_title
    };
    activity.generic_activity = generic_activity;
})(activity || (activity = {}));
var activity;
(function (activity) {
    class node_student_assignment extends activity.node_root {
        is_teacher(uid) {
            return this.activity.is_owner(uid) || this.activity.is_associate(uid);
        }
        is_student(uid) {
            return ((uid === this.uid));
        }
        change_workflow(wfn) {
            if (this.workflow === wfn)
                return;
            let dpn = node.drupal_field_name('workflow');
            let m = {
                nid: api.rest.encapsulate_single_value(this.nid),
                type: api.rest.encapsulate_single_target_id(this.type),
            };
            m[dpn] = api.rest.encapsulate_single(wfn, api.rest.value_encapsulator);
            return api.rest.saveAsync(node.node_url(this.nid), m).then((o) => { this.load_field(o, 'workflow', this.__loaded_values); });
        }
    }
    node_student_assignment.def = {
        type: "student_assignment",
        fields: {
            'activity': { type: "target" },
            'evaluation': { type: "w", default: "" },
            'appreciation': { type: "w", default: "" },
            'workflow': { type: "ro", default: 100 },
        },
        ro_title: true,
    };
    activity.node_student_assignment = node_student_assignment;
})(activity || (activity = {}));
var activity;
(function (activity) {
    function wf_to_int(wf) {
        switch (wf) {
            case 'wf1': return 100;
            case 'wf2': return 200;
            case 'wf3': return 300;
        }
        throw new activity.activity_error('Workflow code ' + wf + ' hors limites.');
    }
    activity.wf_to_int = wf_to_int;
    function int_to_wf(wfn) {
        if (wfn >= 100) {
            if (wfn < 200)
                return "wf1";
            if (wfn < 300)
                return "wf2";
            if (wfn < 400)
                return "wf3";
        }
        throw new activity.activity_error('Workflow value ' + wfn + ' hors limites.');
    }
    activity.int_to_wf = int_to_wf;
    class nodes_manager {
        constructor(n, cu, m, su) {
            this.__dirty_v = false;
            this.__dirty_change_handler = () => { };
            this.n = n;
            this.__cu = cu;
            this.mode = m;
            switch (m) {
                case "view":
                case "create":
                    this.n_activity = n;
                    this.n_activity.on_dirty_change = () => { this.__triggers_dirty_change(); };
                    break;
                case "assignment":
                case "review":
                    this.n_student_assignment = n;
                    this.n_activity = this.n_student_assignment.activity;
                    this.__su = su;
                    this.n_student_assignment.on_dirty_change = () => { this.__triggers_dirty_change(); };
                    break;
            }
            this.__load_config();
        }
        get cu() { return this.__cu; }
        get su() { return this.__su; }
        __load_config() {
            this.__config = JSON.parse(this.n_activity.config);
            this.__dirty_config = false;
        }
        get is_dirty() { return this.n.is_dirty || this.n.has_dirty_targets || this.__dirty_config; }
        set on_dirty_change(h) { this.__dirty_change_handler = h; }
        __triggers_dirty_change() {
            let v = this.is_dirty;
            if (this.__dirty_v !== v) {
                this.__dirty_v = v;
                this.__dirty_change_handler(v);
            }
        }
        has_mode(modes) {
            for (let m of modes) {
                if (this.mode === m)
                    return true;
            }
            return false;
        }
        get tc() { return (this.mode === "create"); }
        ;
        get TC() {
            if (this.tc)
                return true;
            else
                throw new activity.activity_error("Le mode ne permet pas cette opération");
        }
        get tr() { return (this.mode === "review"); }
        get TR() {
            if (this.tr)
                return true;
            else
                throw new activity.activity_error("Le mode ne permet pas cette opération");
        }
        get sa_tr() { return ((this.mode === "assignment") || (this.mode === "review")); }
        get SA_TR() {
            if (this.sa_tr)
                return true;
            else
                throw new activity.activity_error("Le mode ne permet pas cette opération");
        }
        get w() {
            switch (this.mode) {
                case 'assignment':
                    let wf = this.n_student_assignment.workflow;
                    if ((wf < 100) || (wf >= 200))
                        return false;
                    else
                        return true;
                case 'create':
                case 'review':
                    return true;
            }
            return false;
        }
        get W() {
            if (this.w)
                return true;
            else
                throw new activity.activity_error("Le mode ne permet pas cette opération");
        }
        get sa_tr_w() {
            if (this.sa_tr)
                return this.w;
            else
                return false;
        }
        get SA_TR_W() {
            if (this.sa_tr_w)
                return true;
            else
                throw new activity.activity_error("Le mode ne permet pas cette opération");
        }
        choose_getter(ifCV, ifAR) {
            switch (this.mode) {
                case "view":
                case "create":
                    return ifCV();
                case "assignment":
                case "review":
                    return ifAR();
            }
        }
        choose_setter(v, ifC, ifAR) {
            switch (this.mode) {
                case "view":
                    throw new activity.activity_error("Le mode ne permet pas cette opération");
                case "create":
                    ifC(v);
                    break;
                case "assignment":
                case "review":
                    this.SA_TR_W;
                    ifAR(v);
                    break;
            }
        }
        get title() { return this.n_activity.title || ""; }
        set title(v) { this.TC && (this.n_activity.title = v); }
        get description() { return this.n_activity.description; }
        set description(v) { this.TC && (this.n_activity.description = v); }
        get status_clonable() { return this.n_activity.status_clonable; }
        set status_clonable(v) {
            this.TC;
            if (this.create_mode !== 'teacher')
                throw new activity.activity_error("Le mode ne permet pas cette opération");
            this.n_activity.status_clonable = v;
        }
        get status_shared() { return this.n_activity.status_shared; }
        set status_shared(v) {
            this.TC;
            if (this.create_mode !== 'teacher')
                throw new activity.activity_error("Le mode ne permet pas cette opération");
            this.n_activity.status_shared = v;
        }
        get code() {
            let c = this.n_activity.code;
            if (c === 'student_activity')
                return false;
            return c;
        }
        get create_mode() {
            if (this.mode !== 'create')
                return false;
            if (this.n_activity.code === 'student_activity')
                return 'student';
            else
                return 'teacher';
        }
        get evaluation() { this.SA_TR; return this.n_student_assignment.evaluation; }
        set evaluation(v) {
            if (this.__sce)
                this.SA_TR_W;
            else
                this.TR;
            this.n_student_assignment.evaluation = v;
        }
        get appreciation() { this.SA_TR; return this.n_student_assignment.appreciation; }
        set appreciation(v) { this.TR && (this.n_student_assignment.appreciation = v); }
        get workflow() { this.SA_TR; return int_to_wf(this.n_student_assignment.workflow); }
        set workflow(v) {
            let msg = "Assigner une valeur à la propriété workflow est interdit. Utilisez la méthode asynchrone change_workflow à la place.";
            console.log(msg);
            throw new activity.activity_error(msg);
        }
        get attached_files() { return this.n_activity.attached_files; }
        change_workflow(v) {
            this.SA_TR_W;
            let wfn = wf_to_int(v);
            if (this.mode === "assignment") {
                if ((wfn < 100) || (wfn >= 300))
                    throw new activity.activity_error("Le mode ne permet pas cette opération");
            }
            if (this.n_student_assignment.workflow === wfn)
                return;
            return this.__retry_if_fail([() => this.n_student_assignment.change_workflow(wfn)]);
        }
        get return_url() {
            if ((this.mode === 'create') || (this.mode === 'assignment')) {
                return '/web/my';
            }
            else if (this.mode === 'review') {
                return '/web/assignments/' + this.n_activity.nid;
            }
            else if (this.mode === 'view') {
                return '/web/bibliotheque';
            }
            else if (this.mode === 'detached') {
                return '/web/';
            }
        }
        get config_url() {
            if (this.mode !== 'create')
                return false;
            return '/web/node/' + this.n_activity.nid + '/edit';
        }
        get_config(cn) {
            let val = this.__config[cn];
            if (val == null)
                return this.__default_config[cn];
            return val;
        }
        set_config(cn, val) {
            if (this.TC) {
                this.__config[cn] = val;
                this.__dirty_config = true;
                this.__triggers_dirty_change();
            }
        }
        __check_current_user() {
            return user.get_me().then((cu) => (cu.uid === this.cu.uid));
        }
        async __retry_if_fail(actions) {
            try {
                while (actions.length > 0) {
                    await (actions[0])();
                    actions.shift();
                }
            }
            catch (e) {
                if (e instanceof xhr.status_error) {
                    if (!await this.__check_current_user()) {
                        let msg = 'L\'utilisateur actuellement connecté à Capytale n\'est plus celui qui a ouvert cette activité.';
                        console.log(msg);
                        throw new activity.user_changed_error(msg);
                    }
                    api.clear_csrf_tk();
                    while (actions.length > 0) {
                        await (actions[0])();
                        actions.shift();
                    }
                }
                else {
                    throw e;
                }
            }
        }
        prepare_for_save(actions) {
            if (this.__dirty_config) {
                this.TC;
                this.n_activity.config = JSON.stringify(this.__config);
                this.__dirty_config = false;
            }
            switch (this.mode) {
                case 'review':
                    actions.push(() => this.n_student_assignment.save());
                    break;
                case 'assignment':
                    this.SA_TR_W;
                    actions.push(() => this.n_student_assignment.save());
                    break;
                case 'create':
                    actions.push(() => this.n_activity.save().then(() => { this.__load_config(); }));
                    break;
                default:
                    throw new activity.activity_error("Le mode ne permet pas de sauvegarder.");
            }
        }
        save() {
            const actions = [];
            this.prepare_for_save(actions);
            return this.__retry_if_fail(actions);
        }
    }
    activity.nodes_manager = nodes_manager;
    let activity_map = {};
    function declare_activity(manager_class) {
        activity_map[manager_class.def.activity_type] = manager_class;
        let default_config = manager_class.def.default_config;
        if (default_config == null)
            default_config = {};
        Object.defineProperty(manager_class.prototype, "__default_config", {
            value: default_config,
            writable: false,
        });
        Object.defineProperty(manager_class.prototype, "activity_type", {
            value: manager_class.def.activity_type,
            writable: false,
        });
        let sce = manager_class.def.student_can_evaluate;
        if (sce == null)
            sce = false;
        Object.defineProperty(manager_class.prototype, "__sce", {
            value: sce,
            writable: false,
        });
    }
    activity.declare_activity = declare_activity;
    async function load(id) {
        let cu = await user.get_me();
        let su;
        let n = await (node.load(id, true, true));
        if (!(n instanceof activity.node_root)) {
            throw new activity.activity_error("Cet id ne correspond pas à une activité.");
        }
        let m;
        let manager_class;
        let def;
        if (n instanceof activity.node_activity) {
            manager_class = activity_map[n.activity_type];
            if (!manager_class)
                throw new activity.activity_error("Type d'activité inconnu.");
            def = manager_class.def;
            if (n.is_owner(cu.uid) || n.is_associate(cu.uid)) {
                m = 'create';
            }
            else {
                m = 'view';
            }
        }
        else if (n instanceof activity.node_student_assignment) {
            if (n.activity.code === 'student_activity')
                throw new activity.activity_error("Cette acivité a été crée par un élève, elle ne peut pas être clonée.");
            manager_class = activity_map[n.activity.activity_type];
            if (!manager_class)
                throw new activity.activity_error("Type d'activité inconnu.");
            def = manager_class.def;
            if (n.is_student(cu.uid)) {
                m = 'assignment';
                su = cu;
            }
            else if (n.is_teacher(cu.uid)) {
                m = 'review';
                let _su = await user.get_infos(n.uid);
                if (false === _su)
                    console.log('Impossible de récupérer l\'identité de l\'élève.');
                else
                    su = _su;
            }
            else {
                throw new activity.activity_error("Accès non autorisé.");
            }
        }
        else {
            throw new activity.activity_error("Cet id ne correspond pas à une activité.");
        }
        return new manager_class(n, cu, m, su);
    }
    activity.load = load;
})(activity || (activity = {}));
var activity;
(function (activity) {
    class ui_manager {
        constructor(nm) {
            this.__nm = nm;
            nm.on_dirty_change = () => this.refresh_dirty();
            this.__confirm_dlg = new confirm_dlg((visible, msg, ok_label) => this.__controllers.set_confirm_dlg_state(visible, msg, ok_label));
            this.__controllers = {};
            this.__action = {
                confirm_ok_click: () => { this.__confirm_dlg.ok(); },
                confirm_cancel_click: () => { this.__confirm_dlg.cancel(); },
                save_click: () => { this.do_save(); },
                wf_click: (wf) => { this.do_change_workflow(wf); }
            };
            window.onbeforeunload = () => {
                if (this.__nm.is_dirty)
                    return "Unsaved changes will be lost. Close anyway?";
            };
        }
        get nm() { return this.__nm; }
        ;
        get controllers() { return this.__controllers; }
        ;
        get actions() { return this.__action; }
        ;
        async do_save(skip = false) {
            if (!this.__nm.w)
                return;
            if ((!skip) && (this.__nm.is_dirty) && (this.__nm.mode === 'review') && (this.__nm.workflow === 'wf1')) {
                if (!await this.__confirm_dlg.confirm('Ce travail n\'a pas encore été rendu.<br>Voulez vous continuer&nbsp;?', 'Enregistrer'))
                    return;
            }
            this.__controllers.set_save_btn_busy(true);
            try {
                await this.__alert_if_fail(() => this.__nm.save());
            }
            finally {
                this.__controllers.set_save_btn_busy(false);
            }
        }
        async do_change_workflow(wf) {
            let owf = this.__nm.workflow;
            if (wf === owf)
                return;
            if (this.__nm.mode === 'assignment') {
                if ((owf !== 'wf1') || (wf !== 'wf2'))
                    throw new activity.activity_error('Le mode ne permet pas cette opération');
                if (this.__nm.is_dirty) {
                    if (!await this.__confirm_dlg.confirm('Pour rendre votre travail, vous devez enregister vos modifications.<br>Voulez-vous continuer&nbsp;?', 'Enregistrer'))
                        return;
                    await this.do_save();
                }
                if (!await this.__confirm_dlg.confirm('Vous allez rendre votre travail. Vous ne pourrez plus le modifier.<br>Voulez-vous continuer&nbsp;?', 'Rendre'))
                    return;
            }
            else if (this.__nm.mode === 'review') {
                if (owf === 'wf1') {
                    if (wf === 'wf2') {
                        if (!await this.__confirm_dlg.confirm('Ce travail n\'a pas encore été rendu.<br>Si vous forcez le statut <em>rendu</em>, ce travail ne sera plus modifiable. Vous pourrez toujours remettre le statut <em>en cours</em> par la suite.<br>Voulez-vous forcer le statut <em>rendu</em>&nbsp;?', 'Forcer'))
                            return;
                    }
                    else if (wf === 'wf3') {
                        if (this.__nm.is_dirty) {
                            if (!await this.__confirm_dlg.confirm('Vos modifications en cours seront enregistrées.<br>Vous allez forcer le statut <em>corrigé</em>. Le travail ne sera plus modifiable mais vous pourrez toujours remettre le statut <em>en cours</em> par la suite.<br>Voulez-vous enregistrer vos modifications et forcer le statut <em>corrigé</em>&nbsp;?', 'Continuer'))
                                return;
                            await this.do_save(true);
                        }
                        else {
                            if (!await this.__confirm_dlg.confirm('Ce travail n\'a pas encore été rendu. Si vous forcez le statut <em>corrigé</em>, ce travail ne sera plus modifiable. Vous pourrez toujours remettre le statut <em>en cours</em> par la suite.<br>Voulez-vous forcer le statut <em>corrigé</em>&nbsp;?', 'Forcer'))
                                return;
                        }
                    }
                }
                else if ((owf === 'wf2') || (owf === 'wf3')) {
                    if (wf === 'wf1') {
                        if (!await this.__confirm_dlg.confirm('Vous allez remettre ce travail <em>en cours</em>. Il sera à nouveau modifiable.<br>Voulez-vous continuer&nbsp;?', 'Continuer'))
                            return;
                    }
                    else if (wf === 'wf3') {
                        if (this.__nm.is_dirty) {
                            if (!await this.__confirm_dlg.confirm('Vos modifications en cours seront enregistrées.<br>Le travail sera indiqué comme <em>corrigé</em>.<br>Voulez-vous continuer&nbsp;?', 'Continuer'))
                                return;
                            await this.do_save(true);
                        }
                    }
                }
            }
            else {
                throw new activity.activity_error('Le mode ne permet pas cette opération');
            }
            try {
                this.__controllers.set_wf_control_busy(true);
                await this.__alert_if_fail(() => this.__nm.change_workflow(wf));
            }
            finally {
                this.__controllers.set_wf_control_busy(false);
                this.refresh_wf_control();
                if (!this.__nm.w)
                    this.__controllers.disable_activity_controls();
                this.refresh_dirty();
            }
        }
        refresh_wf_control() {
            let wf = this.__nm.workflow;
            let state;
            if (this.__nm.sa_tr) {
                state = wf;
            }
            else {
                throw new activity.activity_error('Opération impossible dans ce mode');
            }
            this.__controllers.set_wf_control_state(state);
        }
        refresh_dirty() {
            let dirty = this.__nm.is_dirty;
            let state;
            if (this.__nm.w) {
                state = dirty ? 'dirty' : 'enabled';
            }
            else {
                state = 'locked';
            }
            this.__controllers.set_save_btn_state(state);
        }
        async __alert_if_fail(action) {
            while (true) {
                try {
                    await action();
                    return true;
                }
                catch (e) {
                    let msg;
                    if (e instanceof activity.user_changed_error) {
                        msg = 'l\'opération a échoué.<br>L\'utilisateur connecté à Capytale a changé.<br>Veuillez vérifier.';
                    }
                    else if (e instanceof user.not_connected_error) {
                        msg = 'l\'opération a échoué.<br>Vous n\'êtes pas connecté à Capytale.<br>Veuillez vérifier.';
                    }
                    else if (e instanceof xhr.network_error) {
                        msg = 'l\'opération a échoué.<br>Veuillez vérifier votre connexion.';
                    }
                    else {
                        msg = 'l\'opération a échoué.<br>' + e.name + '<br>' + e.message;
                    }
                    if (!await this.__confirm_dlg.confirm(msg, 'Réessayer'))
                        return false;
                }
            }
        }
        get save_lm() {
            if (this.__nm.mode === 'assignment') {
                return this.__nm.w ? 'enabled' : 'locked';
            }
            else if (this.__nm.has_mode(['create', 'review'])) {
                return 'enabled';
            }
            else {
                return 'hidden';
            }
        }
        get assignment_lm() {
            switch (this.__nm.mode) {
                case 'assignment': return 'assignment';
                case 'review': return 'review';
            }
            return 'not';
        }
        get backend_lm() {
            if (this.__nm.mode === 'detached')
                return 'hidden';
            else
                return 'visible';
        }
        get create_lm() {
            if (this.__nm.mode === 'create')
                return 'editable';
            else
                return 'view';
        }
        get creator_lm() {
            if (this.__nm.mode === 'create') {
                return this.__nm.create_mode;
            }
            return 'not';
        }
    }
    activity.ui_manager = ui_manager;
    class confirm_dlg {
        constructor(sw) {
            this.confirm_pending = false;
            this.sw = sw;
        }
        confirm(msg = 'Confirmez.', ok_lablel = "OK") {
            if (this.confirm_pending)
                throw new Error('Confirm() n\'est pas réentrant.');
            this.confirm_pending = true;
            return new Promise((rs, rj) => {
                this.rs = rs;
                this.rj = rj;
                this.sw(true, msg, ok_lablel);
            });
        }
        ok() {
            if (this.confirm_pending) {
                this.sw(false);
                this.confirm_pending = false;
                this.rs(true);
                this.rs = this.rj = undefined;
            }
        }
        cancel() {
            if (this.confirm_pending) {
                this.sw(false);
                this.confirm_pending = false;
                this.rs(false);
                this.rs = this.rj = undefined;
            }
        }
    }
})(activity || (activity = {}));
