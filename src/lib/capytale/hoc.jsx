import capytaleGUI from "./gui.jsx";

import React from "react";
import Divider from "../../components/divider/divider.jsx";
import { setProjectChanged } from "../../reducers/project-changed";
import { MenuItem, MenuSection } from "../../components/menu/menu.jsx";
import classNames from "classnames";
import { defineMessages, FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import omit from "lodash.omit";
import bindAll from "lodash.bindall";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// not using tree shaking here since it has issue with webpack 4 and uglifyjs
// See https://github.com/terser/terser/issues/50
// and https://fontawesome.com/docs/apis/javascript/tree-shaking#slow-build-times-with-webpack-4-uglify-es-3-and-uglify-js-3
import { faClipboardList } from "@fortawesome/free-solid-svg-icons/faClipboardList";
import { faClipboardCheck } from "@fortawesome/free-solid-svg-icons/faClipboardCheck";
import { faEnvelopeOpenText } from "@fortawesome/free-solid-svg-icons/faEnvelopeOpenText";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons/faEnvelope";
import { faListCheck } from "@fortawesome/free-solid-svg-icons/faListCheck";
import { faUserPen } from "@fortawesome/free-solid-svg-icons/faUserPen";

import { sanitize } from "./sanitizer";

import {
    enableProjectSaving,
    disableProjectSaving,
    openDescriptionModal,
    openEvaluationModal,
    openWorkflowModal,
    openAboutModal,
    setMode,
    setCreateMode,
    setWorkflow,
    setTitle,
    setDescriptionContent,
    setAppreciationContent,
    setEvaluationContent,
    setBackLink,
    closeDescriptionModal,
    closeEvaluationModal,
    closeWorkflowModal,
    closeAboutModal,
    setStudentName,
} from "./reducer.jsx";

import DescriptionModal from "./modals/description-modal.jsx";
import EvaluationModal from "./modals/evaluation-modal.jsx";
import WorkflowModal from "./modals/workflow-modal.jsx";
import AboutModal from "./modals/about-modal.jsx";

import stylesMenuBar from "../../components/menu-bar/menu-bar.css";
import stylesMenuBarCapytale from "./menu-bar.css";
import backIcon from "../assets/icon--back.svg";

const ariaMessages = defineMessages({
    description: {
        id: "gui.menuBar.description",
        defaultMessage: "Consigne",
        description: "accessibility text for the description button",
    },
    evaluation: {
        id: "gui.menuBar.evaluation",
        defaultMessage: "Correction",
        description: "accessibility text for the evaluation button",
    },
});

export default function (WrappedComponent) {
    class CapytaleComponent extends React.Component {
        constructor(props) {
            super(props);
            capytaleGUI._setHOC(this);
            bindAll(this, [
                "descriptionModal",
                "evaluationModal",
                "workflowModal",
                "aboutModal",
                "descriptionButton",
                "evaluationButton",
                "workflowButton",
                "backButton",
                "studentName",
            ]);
        }

        /**
         * Fontawesome icons in menubar.
         */
        menuBarIcon(icon) {
            return (
                <div className={stylesMenuBarCapytale.menuBarIcon}>
                    <FontAwesomeIcon
                        icon={icon}
                        className={stylesMenuBar.helpIcon}
                    />
                </div>
            );
        }

        /**
         * About menu item.
         */
        aboutMenuItem() {
            return (
                <MenuSection>
                    <MenuItem onClick={this.props.onOpenAboutModal}>
                        À propos
                    </MenuItem>
                </MenuSection>
            );
        }

        /**
         * Description button to put in the menubar
         */
        descriptionButton() {
            return (
                <>
                    <div
                        aria-label={this.props.intl.formatMessage(
                            ariaMessages.description
                        )}
                        className={classNames(
                            stylesMenuBar.menuBarItem,
                            stylesMenuBar.hoverable
                        )}
                        onClick={this.props.onOpenDescriptionModal}
                    >
                        {this.menuBarIcon(faClipboardList)}
                        <FormattedMessage {...ariaMessages.description} />
                    </div>
                    <Divider className={classNames(stylesMenuBar.divider)} />
                </>
            );
        }

        /**
         * Evaluation button to put in the menubar
         */
        evaluationButton() {
            return ["assignment", "review"].includes(this.props.mode) ? (
                <>
                    <div
                        aria-label={this.props.intl.formatMessage(
                            ariaMessages.evaluation
                        )}
                        className={classNames(
                            stylesMenuBar.menuBarItem,
                            stylesMenuBar.hoverable
                        )}
                        onClick={this.props.onOpenEvaluationModal}
                    >
                        {this.menuBarIcon(faClipboardCheck)}
                        <FormattedMessage {...ariaMessages.evaluation} />
                    </div>
                    <Divider className={classNames(stylesMenuBar.divider)} />
                </>
            ) : null;
        }

        /**
         * Workflow button to put in the menubar
         */
        workflowButton() {
            if (this.props.workflow == null) return;

            const messages = {
                assignment: {
                    wf1: "Rendre le travail",
                    wf2: "Travail rendu",
                    wf3: "Travail corrigé",
                },
                review: {
                    wf1: "Travail non rendu",
                    wf2: "Travail rendu",
                    wf3: "Travail corrigé",
                },
            };
            const message = messages[this.props.mode][this.props.workflow];

            const titles = {
                assignment: {
                    wf1: "Indiquer à l'enseignant que le travail est terminé, attention, vous ne pourrez plus le modifier",
                    wf2: "Le travail a été rendu, il n'est plus modifiable",
                    wf3: "Le travail a été corrigé par l'enseignant, il n'est plus modifiable",
                },
                review: {
                    wf1: "L'élève n'a pas rendu son travail, il peut toujours le modifier, cliquez pour forcer le rendu",
                    wf2: "L'élève a rendu son travail, il ne peut plus le modifier, cliquez pour indiquer qu'il est corrigé ou pour lui permettre de le modifier à nouveau",
                    wf3: "Vous avez indiqué avoir corrigé ce travail, cliquez pour modifier",
                },
            };
            const title = titles[this.props.mode][this.props.workflow];

            const icons = {
                assignment: {
                    wf1: faEnvelopeOpenText,
                    wf2: faEnvelope,
                    wf3: faListCheck,
                },
                review: {
                    wf1: faUserPen,
                    wf2: faEnvelope,
                    wf3: faListCheck,
                },
            };
            const icon = icons[this.props.mode][this.props.workflow];
            const hoverable = !(
                this.props.mode === "assignment" &&
                this.props.workflow !== "wf1"
            );
            return (
                <>
                    <div
                        id="gui.menuBar.workflow"
                        title={title}
                        className={classNames({
                            [stylesMenuBar.menuBarItem]: true,
                            [stylesMenuBar.hoverable]: hoverable,
                        })}
                        onClick={
                            hoverable
                                ? this.props.onOpenWorkflowModal
                                : undefined
                        }
                    >
                        {this.menuBarIcon(icon)}
                        {message}
                    </div>
                    <Divider className={classNames(stylesMenuBar.divider)} />
                </>
            );
        }

        /**
         * Back button to put in the menubar
         */
        backButton() {
            return (
                <>
                    <a
                        id="gui.menuBar.goback"
                        className={classNames(
                            stylesMenuBar.menuBarItem,
                            stylesMenuBar.hoverable
                        )}
                        href={this.props.backLink}
                    >
                        <img
                            className={stylesMenuBar.helpIcon}
                            src={backIcon}
                        />
                    </a>
                </>
            );
        }

        /**
         * Student's name in the menubar.
         */
        studentName() {
            return this.props.mode === "review" ? (
                <div
                    id="gui.menuBar.studentName"
                    className={stylesMenuBar.menuBarItem}
                >
                    <div className={stylesMenuBarCapytale.menuBarStudentName}>
                        {this.props.studentName}
                    </div>
                </div>
            ) : null;
        }

        /**
         * Render description modal.
         */
        descriptionModal() {
            return this.props.descriptionModalVisible ? (
                <DescriptionModal
                    isRtl={this.props.isRtl}
                    title={this.props.title}
                    content={this.props.descriptionContent}
                    onRequestClose={this.props.onRequestCloseDescriptionModal}
                />
            ) : null;
        }

        /**
         * Render evaluation modal.
         */
        evaluationModal() {
            return this.props.evaluationModalVisible ? (
                <EvaluationModal
                    isRtl={this.props.isRtl}
                    contentAppreciation={this.props.appreciationContent}
                    contentEvaluation={this.props.evaluationContent}
                    onRequestClose={this.props.onRequestCloseEvaluationModal}
                />
            ) : null;
        }

        /**
         * Render workflow modal.
         */
        workflowModal() {
            return this.props.workflowModalVisible ? (
                <WorkflowModal
                    isRtl={this.props.isRtl}
                    onRequestClose={this.props.onRequestCloseWorkflowModal}
                    onChangeWorkflow={this.props.onChangeWorkflow}
                />
            ) : null;
        }

        /**
         * Render about modal.
         */
        aboutModal() {
            return this.props.aboutModalVisible ? (
                <AboutModal
                    isRtl={this.props.isRtl}
                    onRequestClose={this.props.onRequestCloseAboutModal}
                />
            ) : null;
        }

        render() {
            const componentProps = omit(this.props, [
                "onRequestCloseDescriptionModal",
                "onRequestCloseEvaluationModal",
                "onRequestCloseWorkflowModal",
                "onRequestCloseAboutModal",
                "onOpenDescriptionModal",
                "onOpenEvaluationModal",
                "onOpenWorkflowModal",
                "onOpenAboutModal",
            ]);
            const toOmit = [
                "descriptionModalVisible",
                "evaluationModalVisible",
                "workflowModalVisible",
                "aboutModalVisible",
                "canSave",
                "mode",
                "createMode",
                "workflow",
                "title",
                "descriptionContent",
                "appreciationContent",
                "evaluationContent",
                "backLink",
                "studentName",
                "onEnableProjectSaving",
                "onDisableProjectSaving",
                "onSetMode",
                "onSetCreateMode",
                "onSetWorkflow",
                "onChangeWorkflow",
                "onSetTitle",
                "onSetDescriptionContent",
                "onSetAppreciationContent",
                "onSetEvaluationContent",
                "onSetBackLink",
                "onSetStudentName",
                "capytalePropsOmit",
            ];
            return (
                <WrappedComponent
                    descriptionModal={this.descriptionModal}
                    evaluationModal={this.evaluationModal}
                    workflowModal={this.workflowModal}
                    aboutModal={this.aboutModal}
                    capytalePropsOmit={toOmit}
                    {...componentProps}
                />
            );
        }
    }

    CapytaleComponent.propTypes = {
        descriptionModalVisible: PropTypes.bool,
        evaluationModalVisible: PropTypes.bool,
        workflowModalVisible: PropTypes.bool,
        aboutModalVisible: PropTypes.bool,
        canSave: PropTypes.bool,
        mode: PropTypes.string,
        createMode: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
        workflow: PropTypes.string,
        title: PropTypes.string,
        descriptionContent: PropTypes.string,
        appreciationContent: PropTypes.string,
        evaluationContent: PropTypes.string,
        backLink: PropTypes.string,
        studentName: PropTypes.string,
        onRequestCloseDescriptionModal: PropTypes.func,
        onRequestCloseEvaluationModal: PropTypes.func,
        onOpenDescriptionModal: PropTypes.func,
        onOpenEvaluationModal: PropTypes.func,
        onOpenWorkflowModal: PropTypes.func,
        onOpenAboutModal: PropTypes.func,
        onSetDescriptionContent: PropTypes.func,
        onSetAppreciationContent: PropTypes.func,
        onSetEvaluationContent: PropTypes.func,
        onSetBackLink: PropTypes.func,
        onSetStudentName: PropTypes.func,
    };
    const mapStateToProps = (state) => ({
        descriptionModalVisible: state.scratchGui.capytale.descriptionModal,
        evaluationModalVisible: state.scratchGui.capytale.evaluationModal,
        workflowModalVisible: state.scratchGui.capytale.workflowModal,
        aboutModalVisible: state.scratchGui.capytale.aboutModal,
        canSave: state.scratchGui.capytale.canSave,
        mode: state.scratchGui.capytale.mode,
        createMode: state.scratchGui.capytale.createMode,
        workflow: state.scratchGui.capytale.workflow,
        title: state.scratchGui.capytale.title,
        descriptionContent: state.scratchGui.capytale.descriptionContent,
        appreciationContent: state.scratchGui.capytale.appreciationContent,
        evaluationContent: state.scratchGui.capytale.evaluationContent,
        backLink: state.scratchGui.capytale.backLink,
        studentName: state.scratchGui.capytale.studentName,
        isRtl: state.locales.isRtl,
    });
    const mapDispatchToProps = (dispatch, ownProps) => ({
        onOpenDescriptionModal: () => dispatch(openDescriptionModal()),
        onOpenEvaluationModal: () => dispatch(openEvaluationModal()),
        onOpenWorkflowModal: () => dispatch(openWorkflowModal()),
        onOpenAboutModal: () => dispatch(openAboutModal()),
        onRequestCloseDescriptionModal: () => {
            if (capytaleGUI.core.createMode) {
                // backup content
                const titleElem = document.getElementById(
                    "gui.descriptionOptIn.title"
                );
                const editor = tinymce.get("gui.descriptionOptIn.editor");
                let dirty = false;
                if (titleElem != null) {
                    const content = sanitize(titleElem.innerText);
                    dispatch(setTitle(content));
                    // set dirty
                    if (content !== capytaleGUI.core.title) dirty = true;
                }
                if (editor != null) {
                    const content = sanitize(editor.getContent());
                    dispatch(setDescriptionContent(content));
                    // set dirty
                    if (content !== capytaleGUI.core.description) dirty = true;
                }
                // set dirty
                if (dirty) dispatch(setProjectChanged());
            }
            dispatch(closeDescriptionModal());
        },
        onRequestCloseEvaluationModal: () => {
            if (capytaleGUI.core.mode === "review") {
                // backup contents
                const editorAppreciation = document.getElementById(
                    "gui.evaluationOptIn.editorAppreciation"
                );
                const editorEvaluation = document.getElementById(
                    "gui.evaluationOptIn.editorEvaluation"
                );
                let dirty = false;
                if (editorAppreciation != null) {
                    const appreciation = editorAppreciation.value;
                    dispatch(setAppreciationContent(appreciation));
                    if (appreciation !== capytaleGUI.core.appreciation)
                        dirty = true;
                }
                if (editorEvaluation != null) {
                    const evaluation = editorEvaluation.value;
                    dispatch(setEvaluationContent(evaluation));
                    if (evaluation !== capytaleGUI.core.evaluation)
                        dirty = true;
                }
                // set dirty
                if (dirty) dispatch(setProjectChanged());
            }
            dispatch(closeEvaluationModal());
        },
        onRequestCloseWorkflowModal: () => dispatch(closeWorkflowModal()),
        onRequestCloseAboutModal: () => dispatch(closeAboutModal()),
        onEnableProjectSaving: () => dispatch(enableProjectSaving()),
        onDisableProjectSaving: () => dispatch(disableProjectSaving()),
        onSetMode: (m) => dispatch(setMode(m)),
        onSetCreateMode: (cm) => dispatch(setCreateMode(cm)),
        onSetWorkflow: (w) => dispatch(setWorkflow(w)),
        onChangeWorkflow: (w, projectChanged) => {
            // if project changed and student is submitting his work
            // we should save before changing workflow
            const studentSubmit =
                capytaleGUI.core.mode === "assignment" &&
                capytaleGUI.core.workflow === "wf1" &&
                w === "wf2";
            const saveBefore = studentSubmit && projectChanged;
            (saveBefore ? capytaleGUI.saveAll() : Promise.resolve())
                .then(() => capytaleGUI.core.changeWorkflow(w))
                .then(() => {
                    if (studentSubmit) dispatch(disableProjectSaving());
                    dispatch(setWorkflow(w));
                });
        },
        onSetTitle: (t) => dispatch(setTitle(t)),
        onSetDescriptionContent: (c) => dispatch(setDescriptionContent(c)),
        onSetAppreciationContent: (c) => dispatch(setAppreciationContent(c)),
        onSetEvaluationContent: (c) => dispatch(setEvaluationContent(c)),
        onSetBackLink: (c) => dispatch(setBackLink(c)),
        onSetStudentName: (c) => dispatch(setStudentName(c)),
    });
    return connect(mapStateToProps, mapDispatchToProps)(CapytaleComponent);
}
