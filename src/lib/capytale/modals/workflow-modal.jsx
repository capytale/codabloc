import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
import classNames from "classnames";
import React from "react";
import { connect } from "react-redux";

import Box from "../../../components/box/box.jsx";
import MessageModal from "./message-modal.jsx";

import styles from "./workflow-modal.css";

const WorkflowModal = (props) => {
    // select title
    const titles = {
        assignment: {
            wf1: "Confirmer le rendu du travail",
        },
        review: {
            wf1: "Forcer le rendu du travail",
            wf2: "Modifier l'état de l'activité",
            wf3: "Modifier l'état de l'activité",
        },
    };
    const title = titles[props.mode][props.workflow];

    // select question
    const questions = {
        assignment: {
            wf1: "Êtes-vous sûr de vouloir rendre votre travail ? Il ne sera plus modifiable.",
        },
        review: {
            wf1: "Êtes-vous sûr de vouloir imposer le rendu de ce travail ? L'élève ne pourra plus le modifier.",
            wf2: "L'élève a rendu son travail. Que voulez-vous faire ?",
            wf3: "Ce travail est actuellement indiqué comme étant corrigé. Que voulez-vous faire ?",
        },
    };
    const question = questions[props.mode][props.workflow];

    const changeAndClose = (wf) => () => {
        props.onChangeWorkflow(wf, props.projectChanged);
        props.onRequestClose();
    };

    // build buttons
    const buttons = {
        assignment: {
            wf1: [
                {
                    text: props.projectChanged
                        ? "Enregistrer et rendre"
                        : "Rendre le travail",
                    handler: changeAndClose("wf2"),
                },
            ],
        },
        review: {
            wf1: [{ text: "Confirmer", handler: changeAndClose("wf2") }],
            wf2: [
                {
                    text: "Indiquer que ce travail a été corrigé",
                    handler: changeAndClose("wf3"),
                },
                {
                    text: `Revenir au statut "travail non rendu" (l'élève pourra de nouveau modifier son travail)`,
                    handler: changeAndClose("wf1"),
                },
            ],
            wf3: [
                {
                    text: `Revenir au statut "travail rendu"`,
                    handler: changeAndClose("wf2"),
                },
                {
                    text: `Revenir au statut "travail non rendu" (l'élève pourra de nouveau modifier son travail)`,
                    handler: changeAndClose("wf1"),
                },
            ],
        },
    };
    const bottom = buttons[props.mode][props.workflow].map((v, i) => (
        <button key={i} className={styles.okButton} onClick={v.handler}>
            {v.text}
        </button>
    ));
    bottom.push(
        <button
            key={bottom.length}
            className={styles.okButton}
            onClick={props.onRequestClose}
        >
            Annuler
        </button>
    );

    return (
        <MessageModal
            id="workflow-modal"
            title={title}
            onRequestClose={props.onRequestClose}
        >
            <Box className={styles.body}>
                <Box className={styles.activityArea}>{question}</Box>
                <Box className={styles.bottomArea}>
                    <Box
                        className={classNames(
                            styles.bottomAreaItem,
                            styles.buttonRow
                        )}
                    >
                        {bottom}
                    </Box>
                </Box>
            </Box>
        </MessageModal>
    );
};

WorkflowModal.propTypes = {
    onChangeWorkflow: PropTypes.func,
    onRequestClose: PropTypes.func,
    mode: PropTypes.string,
    createMode: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    workflow: PropTypes.string,
    projectChanged: PropTypes.bool,
};

const mapStateToProps = (state) => ({
    mode: state.scratchGui.capytale.mode,
    createMode: state.scratchGui.capytale.createMode,
    workflow: state.scratchGui.capytale.workflow,
    projectChanged: state.scratchGui.projectChanged,
});

export default connect(mapStateToProps)(WorkflowModal);
