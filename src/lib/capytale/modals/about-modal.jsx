import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
import classNames from "classnames";
import React from "react";
import { connect } from "react-redux";

import Box from "../../../components/box/box.jsx";
import MessageModal from "./message-modal.jsx";

import styles from "./workflow-modal.css";

const commitHash = window.codablocSysInfo["commit-hash"];
const commitDate = window.codablocSysInfo["commit-date"];

const AboutModal = (props) => {
    return (
        <MessageModal
            id="about-modal"
            title="À propos de Codabloc"
            onRequestClose={props.onRequestClose}
        >
            <Box className={styles.body}>
                <Box className={styles.activityArea}>
                    <Box style={{ textAlign: "center", marginBottom: "1ex" }}>
                        <b>Informations de version :</b>
                    </Box>
                    <Box>
                        Commit : <code>{commitHash}</code>
                    </Box>
                    <Box>
                        Date : <code>{commitDate}</code>
                    </Box>
                </Box>
                <Box className={styles.bottomArea}>
                    <Box
                        className={classNames(
                            styles.bottomAreaItem,
                            styles.buttonRow
                        )}
                    >
                        <button
                            className={styles.okButton}
                            onClick={props.onRequestClose}
                        >
                            Fermer
                        </button>
                    </Box>
                </Box>
            </Box>
        </MessageModal>
    );
};

AboutModal.propTypes = {
    onRequestClose: PropTypes.func,
};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(AboutModal);
