import PropTypes from "prop-types";
import React from "react";
import {
    defineMessages,
    injectIntl,
    intlShape,
    FormattedMessage,
} from "react-intl";
import ReactModal from "react-modal";
import { connect } from "react-redux";
import TextareaAutosize from "react-textarea-autosize";

import Box from "../../../components/box/box.jsx";
import { sanitize } from "../sanitizer";
import styles from "./evaluation-modal.css";

const messages = defineMessages({
    label: {
        id: "gui.evaluationOptIn.label",
        defaultMessage: "Activity evaluation",
        description: "Codabloc 3.0 evaluation modal label - for accessibility",
    },
    title1: {
        defaultMessage: "Appréciation",
        description: "Activity evaluation title1",
        id: "gui.evaluationOptIn.title1",
    },
    title2: {
        defaultMessage: "Évaluation",
        description: "Activity evaluation title2",
        id: "gui.evaluationOptIn.title2",
    },
});

/* helper function to take care of previous
 * content generated with rich text editor */
const richContentToPlainText = (src) => {
    if (src.startsWith("<")) {
        src = sanitize(src)
            .replace(/<(?:br|\/div|\/p)>/g, "\n")
            .replace(/<.*?>/g, "");
        const textArea = document.createElement("textarea");
        textArea.innerHTML = src;
        src = textArea.value;
    }
    return src;
};

class EvaluationModal extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ReactModal
                isOpen
                className={styles.modalContent}
                contentLabel={this.props.intl.formatMessage(messages.label)}
                overlayClassName={styles.modalOverlay}
                onRequestClose={this.props.onRequestClose}
            >
                <div dir={this.props.isRtl ? "rtl" : "ltr"}>
                    {this.props.mode === "review" ? (
                        <>
                            <Box className={styles.illustration}>
                                {this.props.studentName}
                            </Box>
                            <Box className={styles.divider} />
                        </>
                    ) : null}
                    <Box className={styles.illustration}>
                        <FormattedMessage {...messages.title1} />
                    </Box>
                    <Box className={styles.body}>
                        <TextareaAutosize
                            id="gui.evaluationOptIn.editorAppreciation"
                            className={styles.editzone}
                            defaultValue={richContentToPlainText(
                                this.props.contentAppreciation
                            )}
                            readOnly={this.props.mode !== "review"}
                        />
                    </Box>
                    <Box className={styles.illustration}>
                        <FormattedMessage {...messages.title2} />
                    </Box>
                    <Box className={styles.body}>
                        <TextareaAutosize
                            id="gui.evaluationOptIn.editorEvaluation"
                            className={styles.editzone}
                            defaultValue={richContentToPlainText(
                                this.props.contentEvaluation
                            )}
                            readOnly={this.props.mode !== "review"}
                        />
                    </Box>
                </div>
            </ReactModal>
        );
    }
}

EvaluationModal.propTypes = {
    intl: intlShape.isRequired,
    isRtl: PropTypes.bool,
    studentName: PropTypes.string,
    contentAppreciation: PropTypes.string,
    contentEvaluation: PropTypes.string,
    onRequestClose: PropTypes.func,
    mode: PropTypes.string,
    createMode: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    workflow: PropTypes.string,
};

const mapStateToProps = (state) => ({
    studentName: state.scratchGui.capytale.studentName,
    mode: state.scratchGui.capytale.mode,
    createMode: state.scratchGui.capytale.createMode,
    workflow: state.scratchGui.capytale.workflow,
});

export default connect(mapStateToProps)(injectIntl(EvaluationModal));
