import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
import classNames from "classnames";
import React from "react";

import Box from "../../../components/box/box.jsx";
import Modal from "../../../containers/modal.jsx";

import styles from "../../../components/connection-modal/connection-modal.css";

const MessageModal = (props) => (
    <Modal
        className={styles.modalContent}
        contentLabel={props.title}
        headerClassName={styles.header}
        id={props.id}
        onRequestClose={props.onRequestClose}
    >
        <Box className={styles.body}>{props.children}</Box>
    </Modal>
);

export default MessageModal;
