import React from "react";

import { Editor as TinyMCEEditor } from "@tinymce/tinymce-react";

import tinymce from "tinymce/tinymce";
import "tinymce/themes/silver";
import "tinymce/icons/default";
import "tinymce/plugins/autoresize/plugin";
import "tinymce/plugins/code/plugin";
import "tinymce/models/dom/model";

import "!!file-loader?outputPath=chunks/skins/ui/oxide&name=[name].[ext]!tinymce/skins/ui/oxide/skin.min.css";
import "!!file-loader?outputPath=chunks/skins/ui/oxide&name=[name].[ext]!tinymce/skins/ui/oxide/content.min.css";
import "!!file-loader?outputPath=chunks/skins/content/default/&name=[name].[ext]!tinymce/skins/content/default/content.css";

export function Editor(props) {
    return (
        <TinyMCEEditor
            id={props.id}
            initialValue={props.content}
            init={{
                //content_css: false,
                menubar: false,
                statusbar: false,
                branding: false,
                toolbar:
                    "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | code",
                plugins: "autoresize code",
                autoresize_bottom_margin: 0,
                height: "0px",
            }}
        />
    );
}
