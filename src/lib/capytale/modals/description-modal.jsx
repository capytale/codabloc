import PropTypes from "prop-types";
import React from "react";
import {
    defineMessages,
    injectIntl,
    intlShape,
    FormattedMessage,
} from "react-intl";
import ReactModal from "react-modal";
import ContentEditable from "react-contenteditable";
import { connect } from "react-redux";

import Box from "../../../components/box/box.jsx";
import styles from "./description-modal.css";
import capytaleGUI from "../gui.jsx";
import { DynamicRichEditor } from "./dynamic-rich-editor.jsx";

const messages = defineMessages({
    label: {
        id: "gui.descriptionOptIn.label",
        defaultMessage: "Activity description",
        description: "Codabloc 3.0 description modal label - for accessibility",
    },
});

class DescriptionModal extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ReactModal
                isOpen
                className={styles.modalContent}
                contentLabel={this.props.intl.formatMessage(messages.label)}
                overlayClassName={styles.modalOverlay}
                onRequestClose={this.props.onRequestClose}
            >
                <div dir={this.props.isRtl ? "rtl" : "ltr"}>
                    <Box className={styles.illustration}>
                        <ContentEditable
                            id="gui.descriptionOptIn.title"
                            html={this.props.title}
                            disabled={!Boolean(this.props.createMode)}
                        />
                    </Box>
                    <Box className={styles.body}>
                        <DynamicRichEditor
                            id="gui.descriptionOptIn.editor"
                            content={this.props.content}
                            rich={Boolean(this.props.createMode)}
                        />
                    </Box>
                </div>
            </ReactModal>
        );
    }
}

DescriptionModal.propTypes = {
    intl: intlShape.isRequired,
    isRtl: PropTypes.bool,
    content: PropTypes.string,
    onRequestClose: PropTypes.func,
    mode: PropTypes.string,
    createMode: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    workflow: PropTypes.string,
};

const mapStateToProps = (state) => ({
    mode: state.scratchGui.capytale.mode,
    createMode: state.scratchGui.capytale.createMode,
    workflow: state.scratchGui.capytale.workflow,
});

export default connect(mapStateToProps)(injectIntl(DescriptionModal));
