/**
 * A module to display content in a div or in a rich editor.
 *
 * TinyMCE is dynamically loaded (only when needed).
 */

import React from "react";

import { sanitize } from "../sanitizer";

/* Dynamically loaded ./tinymce.js */
let tinymce = null;

export function preloadRichEditorModule() {
    if (tinymce != null) return;
    return import("./tinymce.jsx").then((module) => {
        if (tinymce == null) tinymce = module;
    });
}

export class DynamicRichEditor extends React.PureComponent {
    constructor(props) {
        super(props);
        this.html = sanitize(this.props.content);
        const loaded = tinymce != null;
        this.state = { tinymceLoaded: loaded };

        if (this.props.rich && !loaded)
            preloadRichEditorModule().then(() =>
                this.setState({ tinymceLoaded: true })
            );
    }

    render() {
        /* we carrefully use dangerouslySetInnerHTML
           since html content is sanitized in constructor */
        return this.props.rich && this.state.tinymceLoaded ? (
            <tinymce.Editor id={this.props.id} content={this.html} />
        ) : (
            /* id should be useless here (below) */
            <div
                id={this.props.id}
                dangerouslySetInnerHTML={{ __html: this.html }}
            ></div>
        );
    }
}
