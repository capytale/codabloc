import { activity, api, node, xhr } from "./activity";
import { sanitize } from "./sanitizer";

/**
 * A class for interacting with Capytale
 * (wrapper around internal activity.js API)
 */
export default class CapytaleAPI {
    constructor() {
        /**
         * Backuping entity id.
         */
        const paramsKey = "id";
        const url = new URL(window.location.href);
        const entityId = url.searchParams.get(paramsKey);

        /**
         * Internal Capytale API
         */
        class CodablocActivity extends activity.node_activity {
            get activity_type() {
                return "codabloc";
            }
        }
        CodablocActivity.def = {
            type: "activity",
            fields: Object.assign(
                Object.assign({}, activity.node_activity.def.fields),
                {
                    activity_type: {
                        type: "ro",
                    },
                }
            ),
            ro_title: activity.node_activity.def.ro_title,
        };
        node.declare_node(CodablocActivity);

        class CodablocNodeManager extends activity.nodes_manager {
            static def = {
                activity_name: "Codabloc",
                activity_type: "codabloc",
            };
        }
        activity.declare_activity(CodablocNodeManager);

        class CodablocStudentAssignment extends activity.node_student_assignment {}
        node.declare_node(CodablocStudentAssignment);

        this._nodeManager = null;
        this._load = activity.load(entityId).then((nm) => {
            this._nodeManager = nm;
            console.log(`Capytale mode: ${nm.mode}`);
            console.log(`Capytale create mode: ${nm.create_mode}`);
        });
        // Throw loading exception to toplevel
        this._load.catch((e) => {
            throw e;
        });
    }

    /**
     * Entity Id getter.
     */
    get entityId() {
        return this._nodeManager?.n?.nid;
    }

    /**
     * True if loaded, false otherwise.
     */
    get isLoaded() {
        return this._nodeManager != null;
    }

    /**
     * Promise to wait for loading.
     */
    load() {
        return this._load;
    }

    /**
     * Return the Capytale mode:
     *   - null if entity request failed or not loaded
     *   - if request succeded:
     *     - "create"
     *     - "assignment"
     *     - "review"
     *     - "view"
     *     - "detached"
     */
    get mode() {
        return this._nodeManager?.mode;
    }

    /**
     * Return the Capytale create mode:
     *   - false if mode is not a creation one
     *   - "student"
     *   - "teacher"
     */
    get createMode() {
        return this._nodeManager?.create_mode;
    }

    /**
     * Get workflow (undefined if it does not make sens).
     */
    get workflow() {
        if (["assignment", "review"].includes(this.mode))
            return this._nodeManager?.workflow;
        return undefined;
    }

    /**
     * Get if the activity is read only.
     */
    get readOnly() {
        return this._nodeManager?.w === false;
    }

    /**
     * Change workflow.
     */
    changeWorkflow(workflow) {
        return this._nodeManager?.change_workflow(workflow);
    }

    /**
     * Get current (server saved) title (sanitized).
     */
    get title() {
        return sanitize(this._nodeManager?.title);
    }

    /**
     * Get current (server saved) description (sanitized).
     */
    get description() {
        return sanitize(this._nodeManager?.description);
    }

    /**
     * Get current (server saved) appreciation (sanitized).
     */
    get appreciation() {
        // do not sanitize here since it goes to a textarea
        return this._nodeManager?.appreciation;
    }

    /**
     * Get current (server saved) evaluation (sanitized).
     */
    get evaluation() {
        // do not sanitize here since it goes to a textarea
        return this._nodeManager?.evaluation;
    }

    /**
     * Get the return URL (for the back button).
     */
    get returnURL() {
        return this._nodeManager?.return_url;
    }

    /**
     * Get student's name and classe string of the form:
     * "firstname lastname -- classe".
     */
    get studentName() {
        const student = this._nodeManager?.su;
        return `${student?.firstname} ${student?.lastname} — ${student?.classe}`;
    }

    /**
     * Matomo snippet.
     */
    matomoInit() {
        const muuid = this._nodeManager?.cu?.muuid;
        // get siteId
        let siteId = {
            21: "2",
            23: "3",
            27: "4",
        }[window.location.hostname.length.toString()];
        if (siteId == null)
            throw new Error("Internal error initializing matomo.");
        let _paq = (window._paq = window._paq ?? []);
        /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
        _paq.push(["setUserId", muuid]);
        _paq.push(["trackPageView"]);
        _paq.push(["enableLinkTracking"]);
        (function () {
            var u = "https://matomo.ac-paris.fr/";
            _paq.push(["setTrackerUrl", u + "matomo.php"]);
            _paq.push(["setSiteId", siteId]);
            var d = document,
                g = d.createElement("script"),
                s = d.getElementsByTagName("script")[0];
            g.async = true;
            g.src = u + "matomo.js";
            s?.parentNode?.insertBefore(g, s);
        })();
    }

    /**
     * Get content from the File API.
     */
    contentFromFileAPI() {
        // TODO: put this in activity.js
        function getArrayBufferOrNullAsync(url, hdr) {
            return xhr
                .rawRequestAsync("GET", url, null, hdr, [200, 204])
                .then((response) => {
                    if (response.status === 204) return null;
                    return response.arrayBuffer().catch((e) => {
                        console.log(e);
                        throw e;
                    });
                });
        }
        function get_field_async(id, field) {
            return getArrayBufferOrNullAsync(
                api.fields.field_url(id, field),
                api.API_ACCESS_HEADER
            );
        }
        const activityGetter = () =>
            get_field_async(this._nodeManager?.n_activity.nid, "content");
        const studentAssignementGetter = () =>
            get_field_async(
                this._nodeManager?.n_student_assignment.nid,
                "content"
            );
        return this.load().then(() =>
            this._nodeManager?.choose_getter(
                activityGetter,
                // in case of empty student assignement,
                // we fall back to teacher content
                () =>
                    studentAssignementGetter().then(
                        (c) => c ?? activityGetter()
                    )
            )
        );
    }

    /**
     * Save content (blocks) to file API.
     */
    saveContentToFileAPI(content) {
        return api.fields.put_field_async(this.entityId, "content", content);
    }

    /**
     * Save all fields other than content (description, evaluation, ...).
     */
    saveOthers(others) {
        const nodeManager = this._nodeManager;
        if (this.createMode) {
            nodeManager.title = sanitize(others.title);
            nodeManager.description = sanitize(others.description);
        }
        if (this.mode === "review") {
            // do not sanitize here since we use raw text
            // (it comes from and goes to a textarea)
            nodeManager.appreciation = others.appreciation;
            nodeManager.evaluation = others.evaluation;
        }
        nodeManager.__dirty = true;
        return this._nodeManager?.save();
    }

    /**
     * Save all fields + content.
     */
    saveAll(content, others) {
        return this.saveContentToFileAPI(content).then(() =>
            this.saveOthers(others)
        );
    }
}
