import sanitizeHtml from "sanitize-html";

/**
 * Merge two objects (concat lists and merge fields).
 */
function merge(obj1, obj2) {
    if (obj2 == null) return obj1;
    if (obj1 == null) return merge(obj2, obj1);
    // merge arrays
    if (Array.isArray(obj1)) {
        if (!Array.isArray(obj2)) throw Error("Can't merge objects!");
        return [...new Set([...obj1, ...obj2])];
    }
    // merge Objects
    const keys = [...new Set([...Object.keys(obj1), ...Object.keys(obj2)])];
    return Object.fromEntries(keys.map((k) => [k, merge(obj1[k], obj2[k])]));
}

// allowed tags, attributes,...
const allowed = merge(sanitizeHtml.defaults, {
    allowedTags: ["img"],
    allowedSchemes: ["data"],
    allowedAttributes: {
        "*": ["style"],
    },
    allowedStyles: {
        "*": {
            // Match HEX and RGB
            color: [
                /^#(0x)?[0-9a-f]+$/i,
                /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/,
            ],
            "text-align": [/^left$/, /^right$/, /^center$/],
            // Match any number with px, em, or %
            "font-size": [/^\d+(?:px|em|%)$/],
        },
    },
});

/**
 * HTML sanitizer mainly for user submited rich editors content.
 */
export function sanitize(html) {
    if (html == null) return html;
    return sanitizeHtml(html, allowed);
}
