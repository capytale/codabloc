import CapytaleAPI from "./core";
import { preloadRichEditorModule } from "./modals/dynamic-rich-editor.jsx";
import { setProjectUnchanged } from "../../reducers/project-changed";

/**
 * Store the base URL (full URL with filename and QS removed).
 */
export const baseURL = (() => {
    const url = new URL(window.location);
    let pathname = url.pathname;
    pathname = pathname.substring(0, pathname.lastIndexOf("/")) + "/";
    return `${url.origin}/${pathname}`;
})();

class GUI {
    constructor() {
        /* this exposition to global scope
           is for testing/debuging purpose only. */
        window._capytaleTestsProxy = this;

        this._core = new CapytaleAPI();
        this._scratchGUI = undefined;
        this._hoc = undefined;

        this._load = new Promise((resolve, reject) => {
            this._loadResolve = resolve;
            this._loadReject = reject;
        });

        this._load.then(() => {
            // init matomo
            try {
                this._core.matomoInit();
            } catch (e) {}

            /* UI setup */

            // preload rich editor module when necessary
            if (this._core.mode === "review" || this._core.createMode)
                preloadRichEditorModule();

            // set mode
            this._scratchGUI.props.onSetMode(this._core.mode);

            // set createMode
            this._scratchGUI.props.onSetCreateMode(this._core.createMode);

            // set workflow
            this._scratchGUI.props.onSetWorkflow(this._core.workflow);

            // disable project saving if read-only
            if (this._core.readOnly)
                this._scratchGUI.props.onDisableProjectSaving();

            // set title (sanitized)
            this._scratchGUI.props.onSetTitle(this._core.title);

            // set description content (sanitized)
            this._scratchGUI.props.onSetDescriptionContent(
                this._core.description
            );

            // set appreciation/evaluation stuff (sanitized)
            if (["assignment", "review"].includes(this._core.mode)) {
                const { appreciation, evaluation } = this._core;
                this._scratchGUI.props.onSetAppreciationContent(appreciation);
                this._scratchGUI.props.onSetEvaluationContent(evaluation);
            }

            // set student's name
            if (this._core.mode === "review") {
                this._scratchGUI.props.onSetStudentName(this._core.studentName);
            }

            // set link of the back button
            this._scratchGUI.props.onSetBackLink(this._core.returnURL);

            // show description when mode is not "edit" or "review"
            if (!["create", "review"].includes(this._core.mode))
                this._hoc.props.onOpenDescriptionModal();

            // loading pen by default (requested by teachers)
            this.props.vm.extensionManager.loadExtensionURL("pen");
        });
    }

    /**
     * Access scratch GUI.
     */
    setScratchGUI(scratchGUI) {
        this._scratchGUI = scratchGUI;
        this._core
            .load()
            .then(this._loadResolve.bind(this))
            .catch(this._loadReject.bind(this));
    }

    /**
     * Set Capytale HOC.
     */
    _setHOC(hoc) {
        this._hoc = hoc;
    }

    /**
     * HOC getter.
     */
    get hoc() {
        return this._hoc;
    }

    /**
     * Get main React props.
     */
    get props() {
        return this._scratchGUI.props;
    }

    /**
     * Access intenal API bindings.
     */
    get core() {
        return this._core;
    }

    /**
     * Promise to wait for loading.
     */
    load() {
        return this._load;
    }

    /**
     * Get the activity content.
     */
    content() {
        return this._core.contentFromFileAPI();
    }

    /**
     * Save the activity content.
     */
    saveContent(content) {
        return this._core.saveContentToFileAPI(content);
    }

    /**
     * Save all fields other than content (desciption, evaluation, ...).
     */
    saveOthers() {
        return this._core.saveOthers();
    }

    /**
     * Save all fields + content.
     */
    saveAll() {
        const others = {};
        if (this._core.createMode) {
            others.title = this.props.title;
            others.description = this.props.descriptionContent;
        }
        if (this._core.mode === "review") {
            others.appreciation = this.props.appreciationContent;
            others.evaluation = this.props.evaluationContent;
        }
        return this.props.vm
            .saveProjectSb3()
            .then((content) => this._core.saveAll(content, others))
            .then(() => setProjectUnchanged());
    }

    /**
     * Mainly for selenium test purpose to avoid wrong click location.
     */
    disableFlyoutSmoothScrolling() {
        this.props.vm.ScratchBlocks.Flyout.prototype.scrollAnimationFraction = 1;
    }
}

export default new GUI();
